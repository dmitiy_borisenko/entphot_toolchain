#include "lib/EntPhots.h"
#include <cstdio>

int main(int argc, char **argv) {
    if (argc < 2) {
        printf("Specify input files wildcard!\n");
        return 1;
    }

    WfReader rdr(argv[1]);
    WfParser psr;
    Calib clb;
    rdr | psr | clb;
    return 0;
}