#include "Utils.h"
#include <cstdio>
#include <TSystem.h>
#include <TPRegexp.h>
#include <TObjString.h>
#include <TH1.h>
#include <TVirtualPad.h>
#include <TFile.h>
#include <cstdlib>
#include <cfloat>
#include <ROOT/TProcessExecutor.hxx>
#include <TSpectrum.h>
#include <TF1.h>

#define MIN(x, y) ({__typeof__(x) _x=x; __typeof__(y) _y=y; _x<_y ? _x : _y;})


// ConfigParam ////////////////////////////////////////////////////////////////////////////////////
template<typename T> ConfigParam<T>::operator ConvType() const { return val; }
template<> ConfigParam<std::string>::operator const char*() const { return val.data(); }

template<typename T>
std::string cling::printValue(const ConfigParam<T> *par) { return std::to_string((T)*par); }
template<>
std::string cling::printValue(const ConfigParam<std::string> *par) { return (const char*)*par; }

template std::string cling::printValue(const ConfigParam<int> *);
template std::string cling::printValue(const ConfigParam<bool> *);
template std::string cling::printValue(const ConfigParam<double> *);
///////////////////////////////////////////////////////////////////////////////////////////////////



// Hooks //////////////////////////////////////////////////////////////////////////////////////////
TPRegexp re_fname_temp("^\\w*%s\\w*$");
TPRegexp re_word("^\\w+$");

struct Hooks {
    static bool N_BEG_SAMPLES(int n) {
        if (n <= 0) {
            printf("N_BEG_SAMPLES = %d;\tDISCARDED! Should be positive\n", n);
            return false;
        }
        if (Utils::N_SUBRANGES.defined && Utils::SUBRANGE_SIZE.defined 
            && Utils::N_BEG_SAMPLES < Utils::N_SUBRANGES * Utils::SUBRANGE_SIZE) 
        {
            Utils::N_SUBRANGES.defined = false;
            Utils::SUBRANGE_SIZE.defined = false;
            printf("N_BEG_SAMPLES = %d;\tWARNING! N_SUBRANGES and SUBRANGE_SIZE cleared\n", n);
        }
        return true;
    };

    static bool N_SUBRANGES(int n) {
        if (!Utils::N_BEG_SAMPLES.defined) {
            printf("N_SUBRANGES = %d;\tDISCARDED! N_BEG_SAMPLES should be set first\n", n);
            return false;
        }
        if (n <= 0) {
            printf("N_SUBRANGES = %d;\tDISCARDED! Should be positive\n", n);
            return false;
        }
        int tmp = Utils::N_BEG_SAMPLES / n; 
        if (tmp == 0) {
            printf("N_SUBRANGES = %d;\tDISCARDED! Causes zero SUBRANGE_SIZE\n", n);
            return false;
        }
        Utils::SUBRANGE_SIZE.val = tmp;
        Utils::SUBRANGE_SIZE.defined = true;
        return true;
    };

    static bool SUBRANGE_SIZE(int n) {
        if (!Utils::N_BEG_SAMPLES.defined) {
            printf("SUBRANGE_SIZE = %d;\tDISCARDED! N_BEG_SAMPLES should be set first\n", n);
            return false;
        }
        if (n <= 0) {
            printf("SUBRANGE_SIZE = %d;\tDISCARDED! Should be positive\n", n);
            return false;
        }
        int tmp = Utils::N_BEG_SAMPLES / n; 
        if (tmp == 0) {
            printf("SUBRANGE_SIZE = %d;\tDISCARDED! Causes zero N_SUBRANGES\n", n);
            return false;
        }
        Utils::N_SUBRANGES.val = tmp;
        Utils::N_SUBRANGES.defined = true;
        return true;
    };

    template<typename T>
    static bool IS_POSITIVE(T n) {
        if (n <= 0) {
            printf("DISCARDED! Should be positive\n");
            return false;
        }
        return true;
    }

    static bool OUT_DIR(std::string path) {
        Long_t st[]{0, 0, 0, 0};
        gSystem->GetPathInfo(path.data(), st, st+1, st+2, st+3);
        
        if ( !(st[2] & 0b10) ) {
            std::string full_path = gSystem->IsAbsoluteFileName(path.data())
                ? path
                : gSystem->ConcatFileName(gSystem->GetWorkingDirectory().data(), path.data()); 
            printf("OUT_DIR = %s;\tDISCARDED! There's no such dir: %s\n", path.data(), full_path.data());
            return false;
        }
        return true;
    }

    static bool OUT_FNAME_TEMP(std::string temp) {
        if ( !re_fname_temp.Match(temp) ) {
            printf("OUT_FNAME_TEMP = %s;\tDISCARDED! Only word characters with one %%s are allowed\n",
                temp.data()); 
            return false;
        }
        return true;
    }

    static bool IS_WORD(std::string s) {
        if (!re_word.Match(s) ) {
            printf("DISCARDED! Only word characters are allowed\n");
            return false;
        }
        return true;
    }
};
///////////////////////////////////////////////////////////////////////////////////////////////////



// ChStats ////////////////////////////////////////////////////////////////////////////////////////
ChStats::ChStats()
: ampHists{0, 0, 0, 0} {
    ampHists[0] = new TH1F("", "", 1, 0, 1);
}

ChStats::~ChStats() {
    // for (auto h : ampHists) delete h;
}

void ChStats::drawHists() {
    TString old_title = ampHists[0]->GetTitle();
    ampHists[0]->SetTitle("Amplitude");

    ampHists[0]->Draw();
    for (int i=1; i<4; ++i) ampHists[i]->Draw("same");
    gPad->BuildLegend();

    ampHists[0]->SetTitle(old_title);
}

void ChStats::write(const char* sub_dir) {
    TDirectory* old_dir = gDirectory;
    gFile->mkdir(sub_dir, "", true)->cd();

    for (auto h : ampHists) if (h) h->Write("", TObject::kOverwrite);

    gDirectory = old_dir;
}
///////////////////////////////////////////////////////////////////////////////////////////////////



// Utils //////////////////////////////////////////////////////////////////////////////////////////
// Встроенные инструменты
ROOT::TProcessExecutor &Utils::POOL = *new ROOT::TProcessExecutor;
TSpectrum &Utils::SPECTOR = *new TSpectrum(1);  

// Параметры I/O
ConfigParam<std::string> Utils::OUT_DIR(Hooks::OUT_DIR);
ConfigParam<std::string> Utils::OUT_FNAME_TEMP(Hooks::OUT_FNAME_TEMP);
ConfigParam<std::string> Utils::IN_TREE_NAME(Hooks::IS_WORD);
ConfigParam<std::string> Utils::OUT_TREE_NAME(Hooks::IS_WORD);

// Параметры для параметризации сигналов
ConfigParam<int> Utils::N_BEG_SAMPLES(Hooks::N_BEG_SAMPLES);
ConfigParam<int> Utils::N_SUBRANGES(Hooks::N_SUBRANGES);
ConfigParam<int> Utils::SUBRANGE_SIZE(Hooks::SUBRANGE_SIZE);
ConfigParam<int> Utils::SIG_THRES;
ConfigParam<int> Utils::SIG_LEN_NaI(Hooks::IS_POSITIVE<int>);
ConfigParam<int> Utils::SIG_LEN_SCAT(Hooks::IS_POSITIVE<int>);
ConfigParam<bool> Utils::SIG_INVERTED;

// Параметры для фитирования гистограмм
ConfigParam<int> Utils::AMP_NBINS(Hooks::IS_POSITIVE<int>);
ConfigParam<int> Utils::AMP_BG_NITERS(Hooks::IS_POSITIVE<int>);
ConfigParam<int> Utils::AMP_FIT_NBINS(Hooks::IS_POSITIVE<int>);
ConfigParam<double> Utils::AMP_FIT_SIGMAS(Hooks::IS_POSITIVE<double>);

// Параметры отрисовки гистограмм
ConfigParam<int> Utils::CANVAS_WIDTH(Hooks::IS_POSITIVE<int>);
ConfigParam<int> Utils::CANVAS_HEIGHT(Hooks::IS_POSITIVE<int>);
ConfigParam<int> Utils::ROWS_ON_PAGE(Hooks::IS_POSITIVE<int>);


bool Utils::init() {
    // Параметры по-умолчанию для I/O
    OUT_DIR = "./data";
    OUT_FNAME_TEMP = "params_%s";
    IN_TREE_NAME = "adc64_data";
    OUT_TREE_NAME = "adc64_params";
    
    // Параметры по-умолчанию для параметризации сигналов
    N_BEG_SAMPLES = 48;
    N_SUBRANGES = 3;
    SIG_THRES = -1000;
    SIG_LEN_NaI = 50;
    SIG_LEN_SCAT = 5;
    SIG_INVERTED = true;

    // Параметры по-умолчанию для фитирования гистограмм
    AMP_NBINS = 100;
    AMP_BG_NITERS = 20;
    AMP_FIT_NBINS = 5;
    AMP_FIT_SIGMAS = 3.0;

    // Параметры по-умолчанию для отрисовки гистограмм 
    CANVAS_WIDTH = 768;
    CANVAS_HEIGHT = 1024;
    ROWS_ON_PAGE = 5;

    return true;
}
bool Utils::inited = init();


TPRegexp re_fname("[0-9a-z]{8}_([0-9]{8}_[0-9]{6})[.]root$|([^/]+)[.]root$|[^/]+$");

const char* Utils::formOutRootFName(const char* in_fname) {
    TObjArray *m_res = re_fname.MatchS(in_fname);
    const char* out_fname = Form(Utils::OUT_FNAME_TEMP, ((TObjString*) m_res->Last())->GetName());
    return Form("%s/%s.root", (const char*)Utils::OUT_DIR, out_fname);
}

const char* Utils::formOutPdfFName(const char* in_fname) {
    TObjArray *m_res = re_fname.MatchS(in_fname);
    const char* out_fname = ((TObjString*) m_res->Last())->GetName();
    return Form("%s/%s.pdf", (const char*)Utils::OUT_DIR, out_fname);
}


template<bool INV>
void Utils::calcParams(const Frame &frm, FrameParams &par) {
    par.amp = 0;
    par.beg = par.len = -1;
    if (frm.size <= N_BEG_SAMPLES) return;

    // Определить zlvl
    double min_dev = DBL_MAX;
    int zero_lvl;
    for (int i=0; i < N_SUBRANGES; ++i) {
        double dev = 0;
        int sum = 0;
        
        for (int s=i * SUBRANGE_SIZE; s < (i + 1) * SUBRANGE_SIZE; ++s) {
            sum += frm.wf[s];
            dev += frm.wf[s] * frm.wf[s];
        }

        dev = dev * SUBRANGE_SIZE - (double)sum * sum;
        if (dev < min_dev) {
            min_dev = dev;
            zero_lvl = sum;
        }
    }
    div_t r = div(zero_lvl, SUBRANGE_SIZE);
    zero_lvl = INV ? (r.quot - (r.rem<0 ? 1 : 0)) : (r.quot + (r.rem>0 ? 1 : 0));

    // Определение параметров сигнала
    int extrem = 0;
    int max_len = (par.type == NaI) ? SIG_LEN_NaI : SIG_LEN_SCAT;
    for (int s=N_BEG_SAMPLES; s < frm.size; ++s) {
        int v = frm.wf[s] - zero_lvl;

        if (INV && v < SIG_THRES || !INV && v > SIG_THRES) {
            par.beg = s;
            int end = MIN(s + max_len, frm.size);
            par.len = end - par.beg;

            for (int s2=s; s2 < end; ++s2) {
                v = frm.wf[s2] - zero_lvl;
                if (INV && v < extrem || !INV && v > extrem) extrem = v;
            }

            break;
        }
    }
    par.amp = abs(extrem);
}

template void Utils::calcParams<false>(const Frame &frm, FrameParams &par);
template void Utils::calcParams<true>(const Frame &frm, FrameParams &par);


void Utils::fitAmp(const FrameParams &pars, ChStats &stats) {
    TH1* orig = stats.ampHists[0];
    TH1* &smooth = stats.ampHists[1];
    TH1* &bg = stats.ampHists[2];
    TH1* &fit = stats.ampHists[3];

    // Сглаживание
    smooth = (TH1*) orig->Clone(Form("%sSmooth", orig->GetName()));
    smooth->SetTitle("Smoothed");
    smooth->SetLineColor(kYellow);  smooth->SetLineWidth(2);
    smooth->Smooth();

    // Вычисление и вычитание фона
    Option_t* opt = (pars.type == SCAT) ? "nosmoothing;compton" : "nosmoothing";
    bg = SPECTOR.Background(smooth, AMP_BG_NITERS, opt);
    bg->SetNameTitle(Form("%sBg", orig->GetName()), "Background");
    bg->SetLineColor(kGreen);   bg->SetLineWidth(2);
    TH1* woBg = (TH1*) smooth->Clone("woBg");
    woBg->Add(bg, -1);

    // Поиск пика
    SPECTOR.Search(woBg, 2, "nomarkov;nosmoothing;nodraw");
    double peak_x = SPECTOR.GetPositionX()[0];
    double peak_y = SPECTOR.GetPositionY()[0];

    // Пристрелочное фитирование гауссианой
    TF1 gfit("gfit", "gaus");
    gfit.SetParameters(peak_y, peak_x);
    double hrange = woBg->GetBinWidth(1) * AMP_FIT_NBINS;
    woBg->Fit(&gfit, "N0Q", "", peak_x - hrange, peak_x + hrange);

    // Фитирование гауссианой
    hrange = gfit.GetParameter(2) * AMP_FIT_SIGMAS;
    woBg->Fit(&gfit, "N0Q", "", peak_x - hrange, peak_x + hrange);
    delete woBg;

    fit = (TH1*) bg->Clone(Form("%sFit", orig->GetName()));
    fit->SetTitle("bg + gausFit");
    fit->SetLineColor(kRed);
    fit->Add(&gfit);
}
///////////////////////////////////////////////////////////////////////////////////////////////////