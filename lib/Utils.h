#ifndef UTILS_H
#define UTILS_H

#include <string>


template<typename T>
class ConfigParam {
    T val;
    bool defined;
    bool (*setterHook)(T);

public:
    ConfigParam(bool (*hook)(T) = [](T) {return true;})
    : val()
    , defined()
    , setterHook(hook) {} 
    
    ConfigParam &operator=(T v) { 
        if ((*setterHook)(v)) {
            val = v; 
            defined = true;
        }
        return *this; 
    }

    using ConvType = 
        typename std::conditional<std::is_same<T,std::string>::value, const char*, T>::type; 
    operator ConvType() const; 

friend class Hooks;
};

namespace cling {
    template<typename T>
    std::string printValue(const ConfigParam<T> *);
}



struct Frame {
    int16_t size;
    int16_t wf[2048];
};



enum FrameType { NaI, SCAT };
class TH1;

struct FrameParams {
    constexpr static const char *SPEC = "amp/s:beg/S:len/S";

    uint16_t amp;
    int16_t beg;
    int16_t len;

    FrameType type;
};



struct ChStats {
    TH1* ampHists[4]; //[0] - original; [1] - smoothed; [2] - bg; [3] - fit

    ChStats();
    ~ChStats();
    void drawHists();
    void write(const char* sub_dir);
};



namespace ROOT {
    class TProcessExecutor;
}
class TSpectrum;

class Utils {
    static bool inited;
    static bool init(); 

public:
    // Встроенные инструменты
    static ROOT::TProcessExecutor &POOL;
    static TSpectrum &SPECTOR;  

    // Параметры I/O
    static ConfigParam<std::string> OUT_DIR;
    static ConfigParam<std::string> OUT_FNAME_TEMP;
    static ConfigParam<std::string> IN_TREE_NAME;
    static ConfigParam<std::string> OUT_TREE_NAME;

    // Параметры для параметризации сигналов
    static ConfigParam<int> N_BEG_SAMPLES;
    static ConfigParam<int> N_SUBRANGES;
    static ConfigParam<int> SUBRANGE_SIZE; 
    static ConfigParam<int> SIG_THRES;
    static ConfigParam<int> SIG_LEN_NaI;
    static ConfigParam<int> SIG_LEN_SCAT;
    static ConfigParam<bool> SIG_INVERTED;

    // Параметры для фитирования гистограмм
    static ConfigParam<int> AMP_NBINS;
    static ConfigParam<int> AMP_BG_NITERS;
    static ConfigParam<int> AMP_FIT_NBINS;
    static ConfigParam<double> AMP_FIT_SIGMAS;

    // Параметры отрисовки гистограмм
    static ConfigParam<int> CANVAS_WIDTH;
    static ConfigParam<int> CANVAS_HEIGHT;
    static ConfigParam<int> ROWS_ON_PAGE;

    // Вспомогательные методы
    static const char* formOutRootFName(const char* in_fname);
    static const char* formOutPdfFName(const char* in_fname);

    // Логика параметризации сигналов
    template<bool INV>
    static void calcParams(const Frame &, FrameParams &); 
    
    // Логика фитирования амлитудных гистограмм
    static void fitAmp(const FrameParams &, ChStats &); 
};


#ifdef __ROOTCLING__
    #pragma link C++ class FrameParams;
    #pragma link C++ class Utils;
#endif

#endif