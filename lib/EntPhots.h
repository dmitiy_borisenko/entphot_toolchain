#ifndef ENT_PHOTS_H
#define ENT_PHOTS_H

#include "Utils.h"
#include <vector>

const int ARM_SIZE = 17;


class TFileCollection;
class TTree;
typedef std::vector<const char *> NamesVec;
enum ERROR { EMPTY_TREE=0, NO_TREE=-1, BAD_FILE=-2 };

class BaseProcessor {
protected:
    TFileCollection *files;
    TTree *tree;
    int64_t ev;

    BaseProcessor();
    BaseProcessor(const char* files_wcard);
    ~BaseProcessor();

public:
    NamesVec getFNames();
    int64_t getEv() const { return ev; }

    virtual void load(TTree*) = 0;
    bool step();
    virtual void exec(BaseProcessor &prev) {}
    BaseProcessor &operator|(BaseProcessor &next) { next.exec(*this); return next; } 
};



template<class T>
class ProcessorTemplate : public BaseProcessor {
protected:
    T lArm[ARM_SIZE];
    T rArm[ARM_SIZE];

public:
    ProcessorTemplate() {}
    ProcessorTemplate(const char* files_wcard) : BaseProcessor(files_wcard) {}    

    const T &getLeftCh(int ch) const { return lArm[ch]; } 
    const T &getRightCh(int ch) const { return rArm[ch]; } 

    void load(TTree*);
};



class WfReader : public ProcessorTemplate<Frame> {
public:
    WfReader() {}
    WfReader(const char* files_wcard) : ProcessorTemplate(files_wcard) {}
};



class WfParser : public ProcessorTemplate<FrameParams> {
    void init();

public:
    static void showErrorMsg(ERROR err, const char *fname);
    
    WfParser() { init(); }
    WfParser(const char* files_wcard) : ProcessorTemplate(files_wcard) { init(); }

    void exec(BaseProcessor &prev);
};



class Calib : public ProcessorTemplate<ChStats> {
    void init();

public:
    static void showErrorMsg(ERROR err, const char *fname);
    
    Calib() { init(); }
    Calib(const char* files_wcard) : ProcessorTemplate(files_wcard) { init(); }

    void exec(BaseProcessor &prev);
};


#ifdef __ROOTCLING__
    #pragma link C++ class WfReader;
    #pragma link C++ class WfParser;
    #pragma link C++ class Calib;
#endif 

#endif