#include "EntPhots.h"
#include <ROOT/TProcessExecutor.hxx>
#include <TFile.h>
#include <TTree.h>


void WfParser::showErrorMsg(ERROR err, const char *fname) {
    switch (err) {
        case EMPTY_TREE:
            printf("EMTPY_TREE %s in %s!!!\n", (const char*)Utils::IN_TREE_NAME, fname);
        break;
        case NO_TREE:
            printf("NO_TREE %s in %s!!!\n", (const char*)Utils::IN_TREE_NAME, fname);
        break;
        case BAD_FILE:
            printf("BAD_FILE!!! %s is not a ROOT file\n", fname);
    }
}


void WfParser::init() {
    int ch;
    for (ch=0; ch < ARM_SIZE-1; ++ch) {
        lArm[ch].type = NaI;
        rArm[ch].type = NaI;
    }
    lArm[ch].type = SCAT;
    rArm[ch].type = SCAT;
}


void WfParser::exec(BaseProcessor &prev) {
    WfReader &rdr = (WfReader &)prev;
    NamesVec fnames = rdr.getFNames();

    std::vector<int64_t> res 
        = Utils::POOL.Map([&](const char *fname) -> int64_t {
        
        // Открыть выбранный файл
        TFile* f = TFile::Open(fname);
        if (!f) return BAD_FILE;

        // Загрузить дерево из выбранного файла
        TTree* in_tree = (TTree*) f->Get(Utils::IN_TREE_NAME); 
        if (!in_tree) return NO_TREE;
        if (!in_tree->GetEntries()) return EMPTY_TREE;
        rdr.load(in_tree);
        
        // Создать выходной файл
        const char *out_fname = Utils::formOutRootFName(fname);
        TFile out_file(out_fname, "recreate");
        
        // Создать и подготовить выходное дерево
        TTree out_tree(Utils::OUT_TREE_NAME, "parameterization of waveforms");
        int ch;
        for (ch=0; ch < ARM_SIZE-1; ++ch) {
            out_tree.Branch(Form("channel_%d", ch), lArm + ch, FrameParams::SPEC);
            out_tree.Branch(Form("channel_%d", ch + ARM_SIZE-1), rArm + ch, FrameParams::SPEC);
        }
        out_tree.Branch(Form("channel_%d", ch*2), lArm + ch, FrameParams::SPEC);
        out_tree.Branch(Form("channel_%d", ch*2 + 1), rArm + ch, FrameParams::SPEC);

        // Обработать данные
        if (Utils::SIG_INVERTED) {
            while (rdr.step()) {
                for (ch=0; ch < ARM_SIZE; ++ch) {
                    Utils::calcParams<true>(rdr.getLeftCh(ch), lArm[ch]);
                    Utils::calcParams<true>(rdr.getRightCh(ch), rArm[ch]);
                }
                out_tree.Fill();
            }
        } else {
            while (rdr.step()) {
                for (ch=0; ch < ARM_SIZE; ++ch) {
                    Utils::calcParams<false>(rdr.getLeftCh(ch), lArm[ch]);
                    Utils::calcParams<false>(rdr.getRightCh(ch), rArm[ch]);
                }
                out_tree.Fill();
            }
        }

        // Записать выходное дерево        
        out_tree.Write();
        return rdr.getEv() + 1;

    }, fnames);

    int64_t n_evts;
    auto res_it = res.cbegin();
    auto name_it = fnames.cbegin();
    for (n_evts=0; res_it != res.cend(); ++res_it, ++name_it) {
        if (*res_it > 0) {
            n_evts += *res_it;
            files->Add( Utils::formOutRootFName(*name_it) );
        } else showErrorMsg((ERROR)*res_it, *name_it);
    }
    printf("WfParser: %lld evts done.\n", n_evts);  
}