#include "EntPhots.h"
#include <ROOT/TProcessExecutor.hxx>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TAxis.h>
#include <TCanvas.h>


void Calib::showErrorMsg(ERROR err, const char *fname) {
    switch (err) {
        case EMPTY_TREE:
            printf("EMTPY_TREE %s in %s!!!\n", (const char*)Utils::OUT_TREE_NAME, fname);
        break;
        case NO_TREE:
            printf("NO_TREE %s in %s!!!\n", (const char*)Utils::OUT_TREE_NAME, fname);
        break;
        case BAD_FILE:
            printf("BAD_FILE!!! %s is not a ROOT file\n", fname);
    }
}


void Calib::init() {
    int ch;
    for (ch=0; ch < ARM_SIZE-1; ++ch) {
        lArm[ch].ampHists[0]->SetNameTitle( Form("leftNaI#%d_amp", ch+1), Form("Left NaI #%d", ch+1) );
        rArm[ch].ampHists[0]->SetNameTitle( Form("rightNaI#%d_amp", ch+1), Form("Right NaI #%d", ch+1) );
    }
    lArm[ch].ampHists[0]->SetNameTitle( "leftSct_amp", "Left Scatterer" );
    rArm[ch].ampHists[0]->SetNameTitle( "rightSct_amp", "Right Scatterer" );
}


void Calib::exec(BaseProcessor &prev) {
    WfParser &psr = (WfParser &)prev;
    NamesVec fnames = psr.getFNames();

    std::vector<int64_t> res 
        = Utils::POOL.Map([&](const char *fname) -> int64_t {
        
        // Открыть выбранный файл
        TFile* f = TFile::Open(fname, "update");
        if (!f) return BAD_FILE;


        // Загрузить дерево из выбранного файла
        TTree* in_tree = (TTree*) f->Get(Utils::OUT_TREE_NAME); 
        if (!in_tree) return NO_TREE;
        if (!in_tree->GetEntries()) return EMPTY_TREE;
        psr.load(in_tree);


        // Инициализировать гистограммы
        int ch;
        double max_amp;
        for (ch=0; ch < ARM_SIZE-1; ++ch) {
            max_amp = in_tree->GetMaximum(Form("channel_%d.amp", ch));
            lArm[ch].ampHists[0]->GetXaxis()->Set(Utils::AMP_NBINS, 0, max_amp);
            lArm[ch].ampHists[0]->Rebuild();

            max_amp = in_tree->GetMaximum(Form("channel_%d.amp", ch + ARM_SIZE-1));
            rArm[ch].ampHists[0]->GetXaxis()->Set(Utils::AMP_NBINS, 0, max_amp);
            rArm[ch].ampHists[0]->Rebuild();
        }
        max_amp = in_tree->GetMaximum(Form("channel_%d.amp", ch*2));
        lArm[ch].ampHists[0]->GetXaxis()->Set(Utils::AMP_NBINS, 0, max_amp);
        lArm[ch].ampHists[0]->Rebuild();

        max_amp = in_tree->GetMaximum(Form("channel_%d.amp", ch*2 + 1));
        rArm[ch].ampHists[0]->GetXaxis()->Set(Utils::AMP_NBINS, 0, max_amp);
        rArm[ch].ampHists[0]->Rebuild();


        // Заполнить гистограммы
        while (psr.step()) {
            if ( psr.getLeftCh(ARM_SIZE-1).amp ) {
                for (ch=0; ch < ARM_SIZE-1; ++ch)
                    if ( psr.getLeftCh(ch).amp ) lArm[ch].ampHists[0]->Fill( psr.getLeftCh(ch).amp );
                lArm[ch].ampHists[0]->Fill( psr.getLeftCh(ch).amp );
            }
            if ( psr.getRightCh(ARM_SIZE-1).amp ) {
                for (ch=0; ch < ARM_SIZE-1; ++ch)
                    if ( psr.getRightCh(ch).amp ) rArm[ch].ampHists[0]->Fill( psr.getRightCh(ch).amp );
                rArm[ch].ampHists[0]->Fill( psr.getRightCh(ch).amp );
            }
        }


        // Фитировать гистограммы
        for (ch=0; ch < ARM_SIZE; ++ch) {
            Utils::fitAmp(psr.getLeftCh(ch), lArm[ch]);
            Utils::fitAmp(psr.getRightCh(ch), rArm[ch]);
        }


        // Распечатать гистограммы
        TCanvas canva("canva", "", Utils::CANVAS_WIDTH, Utils::CANVAS_HEIGHT);
        canva.Divide(2, Utils::ROWS_ON_PAGE);
        std::string pdf_fname = Utils::formOutPdfFName(fname);

        for (ch=0; ch < ARM_SIZE; ++ch) {
            int quot = ch / Utils::ROWS_ON_PAGE;
            int rem = ch % Utils::ROWS_ON_PAGE;

            if (quot && !rem) {
                canva.Print( quot == 1 ? (pdf_fname + '(').data() : pdf_fname.data() );
                canva.Clear("D");
            }

            canva.cd(rem*2 + 1);
            lArm[ch].drawHists();
            canva.cd(rem*2 + 2);
            rArm[ch].drawHists();
        }

        canva.Print( (pdf_fname + ')').data() );


        // Записать гистограммы
        for (ch=0; ch < ARM_SIZE-1; ++ch) {
            lArm[ch].write(Form("channel_%d", ch));
            rArm[ch].write(Form("channel_%d", ch + ARM_SIZE-1));
        }
        lArm[ch].write(Form("channel_%d", ch*2));
        rArm[ch].write(Form("channel_%d", ch*2 + 1));


        return psr.getEv() + 1;

    }, fnames);

    int64_t n_evts;
    auto res_it = res.cbegin();
    auto name_it = fnames.cbegin();
    for (n_evts=0; res_it != res.cend(); ++res_it, ++name_it) {
        if (*res_it > 0) {
            n_evts += *res_it;
            files->Add(*name_it);
        } else showErrorMsg((ERROR)*res_it, *name_it);
    }
    printf("Calib: %lld evts done.\n", n_evts);  
}