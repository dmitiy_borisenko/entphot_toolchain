#include "EntPhots.h"
#include <TFileCollection.h>
#include <THashList.h>
#include <TFileInfo.h>
#include <TTree.h>


// BaseProcessor //////////////////////////////////////////////////////////////////////////////////
BaseProcessor::BaseProcessor()
: files(new TFileCollection)
, tree(nullptr)
, ev(-1)
{}

BaseProcessor::BaseProcessor(const char* files_wcard) 
: files(new TFileCollection)
, tree(nullptr)
, ev(-1) 
{
    files->Add(files_wcard);
}

BaseProcessor::~BaseProcessor() { delete files; }

NamesVec BaseProcessor::getFNames() {
    NamesVec res;
    for (auto f_info : *files->GetList()) {
        res.push_back( ((TFileInfo *) f_info)->GetCurrentUrl()->GetFile() );
    }
    return res;
}

bool BaseProcessor::step() {
    if (ev + 1 >= tree->GetEntries()) return false;
    return tree->GetEntry(++ev);
}
///////////////////////////////////////////////////////////////////////////////////////////////////



// ProcessorTemplate //////////////////////////////////////////////////////////////////////////////
template<class T>
void ProcessorTemplate<T>::load(TTree *tr) {
    tree = tr;
    ev = -1;

    int ch;
    for (ch=0; ch < ARM_SIZE-1; ++ch) {
        tree->SetBranchAddress(Form("channel_%d", ch), lArm + ch);
        tree->SetBranchAddress(Form("channel_%d", ch + ARM_SIZE-1), rArm + ch);
    }
    tree->SetBranchAddress(Form("channel_%d", ch * 2), lArm + ch);
    tree->SetBranchAddress(Form("channel_%d", ch * 2 + 1), rArm + ch);
}

template void ProcessorTemplate<Frame>::load(TTree*);
template void ProcessorTemplate<FrameParams>::load(TTree*);
template void ProcessorTemplate<ChStats>::load(TTree*);
///////////////////////////////////////////////////////////////////////////////////////////////////