LIB := ./lib
BLD := ./bld
PROJ_DICT := $(BLD)/EntphotTools.cxx
PROJ_LIB := $(BLD)/libEntphotTools.so
PROJ_EXE := $(BLD)/entphots

CLING = rootcling -f $@ -c $^
LIB_COMPILER = g++ -c $< -o $@ -fPIC $(shell root-config --cflags) -I.
LIB_LINKER = g++ -shared $^ -o $@ $(shell root-config --libs) -lSpectrum
EXE_BUILDER = g++ $^ -o $@ -L$(BLD) -l:$(notdir $(PROJ_LIB))

OBJ := $(patsubst $(LIB)/%.cc,$(BLD)/%.o,$(wildcard $(LIB)/*.cc)) $(PROJ_DICT:.cxx=.o)
HDR := $(wildcard $(LIB)/*.h)

$(PROJ_EXE): entphots.cc | $(PROJ_LIB)
	$(EXE_BUILDER)

$(PROJ_LIB): $(OBJ)
	$(LIB_LINKER)

$(BLD)/%.o: $(LIB)/%.cc $(HDR)
	$(LIB_COMPILER)

$(PROJ_DICT:.cxx=.o): $(PROJ_DICT)
	$(LIB_COMPILER)

$(PROJ_DICT): $(HDR) $(BLD)/LinkDef.h
	$(CLING)

$(BLD)/LinkDef.h:
	@touch $@

.PHONY: clean
clean:
ifdef BLD
	@find ./bld/* -exec rm -vf {} \;
endif